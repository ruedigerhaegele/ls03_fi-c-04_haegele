package controller;

import model.User;
import model.persistence.IPersistence;
import model.persistence.IUserPersistence;

/**
 * controller for users
 * @author Clara Zufall, v1.1ff R�diger Haegele
 * 
 */
public class UserController {

	private IUserPersistence userPersistance;
	
	
	public UserController(IPersistence alienDefenceModel) {
		this.userPersistance = alienDefenceModel.getUserPersistance();
	}
	
	public void createUser(User user) {

	}
	
	/**
	 * liest einen User aus der Persistenzschicht und gibt das Userobjekt zur�ck
	 * @param username eindeutige Loginname
	 * @return Userobjekt, null wenn der User nicht existiert
	 */
	public User readUser(String username) {
		User user = this.userPersistance.readUser(username);
		
		if(user != null ) {
			return user;
		} else {
			return null;
		}
	}
	
	public void changeUser(User user) {
		
	}
	
	public void deleteUser(User user) {
		
	}
	
	public boolean checkPassword(String username, String passwort) {
		/*
		User user = this.readUser(username);
		
		if(user != null ) {
			String passwordDB = user.getPassword();
			
			if(passwordDB.equals(passwort) {
				return true;
			}
			return false;
		}
		 */
		
		return false;
	}
}
