import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.SwingConstants;
import javax.swing.JTextPane;
import java.awt.Font;
import javax.swing.JColorChooser;

public class MyGUI extends JFrame {

	private JPanel contentPane;
	 

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MyGUI frame = new MyGUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public MyGUI() {		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 365, 602);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		// Standard-Hintergrundfarbe merken
		Color standardBgColor = contentPane.getBackground();
		
		// Label (durch Funktionen zu ver�ndern)
		JLabel lblHeadline = new JLabel("Dieser Text soll ver�ndert werden.");
		lblHeadline.setHorizontalAlignment(SwingConstants.CENTER);
		lblHeadline.setBounds(10, 11, 331, 61);
		contentPane.add(lblHeadline);
		
		// Hintergrundfarbe �ndern auf...
		JLabel lblNewLabel = new JLabel("Aufgabe 1: Hintergrundfarbe \u00E4ndern");
		lblNewLabel.setBounds(10, 83, 331, 14);
		contentPane.add(lblNewLabel);
		
		// ...rot
		JButton btnRed = new JButton("Rot");
		btnRed.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				buttonRedBg_clicked();				
			}
		});
		btnRed.setBounds(10, 108, 104, 23);
		contentPane.add(btnRed);
		
		// ...gr�n
		JButton btnGreen = new JButton("Gr\u00FCn");
		btnGreen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				buttonGreenBg_clicked();				
			}
		});
		btnGreen.setBounds(124, 108, 103, 23);
		contentPane.add(btnGreen);
		
		// ...blau
		JButton btnBlue = new JButton("Blau");
		btnBlue.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				buttonBlueBg_clicked();				
			}
		});
		btnBlue.setBounds(237, 108, 104, 23);
		contentPane.add(btnBlue);

		// ...gelb
		JButton btnYellow = new JButton("Gelb");
		btnYellow.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				buttonYellowBg_clicked();				
			}
		});
		btnYellow.setBounds(10, 142, 104, 23);
		contentPane.add(btnYellow);
		
		// Standard Hintergrund
		JButton btnStandardcolor = new JButton("Standardfarbe");
		btnStandardcolor.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				buttonGivencolorBg_clicked(standardBgColor);
			}
		});
		btnStandardcolor.setBounds(124, 142, 103, 23);
		contentPane.add(btnStandardcolor);
		
		// selbstgew�hlte Hintergrundfarbe 
		JButton btnChooseColor = new JButton("Farbe w\u00E4hlen...");
		btnChooseColor.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Color ausgewaehlteFarbe = JColorChooser.showDialog(null, "Farbauswahl", null);
				buttonGivencolorBg_clicked(ausgewaehlteFarbe);
			}
		});
		btnChooseColor.setBounds(237, 142, 104, 23);
		contentPane.add(btnChooseColor);
		
		// Text formatieren
		JLabel lblAufgabeText = new JLabel("Aufgabe 2: Text formatieren");
		lblAufgabeText.setBounds(10, 176, 331, 14);
		contentPane.add(lblAufgabeText);
		
		// Font auf Arial
		JButton btnTexttofontarial = new JButton("Arial");
		btnTexttofontarial.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int currentFontSize = lblHeadline.getFont().getSize();
				lblHeadline.setFont(new Font("Arial", Font.PLAIN, currentFontSize));
			}
		});
		btnTexttofontarial.setBounds(10, 201, 104, 23);
		contentPane.add(btnTexttofontarial);
		
		// Font auf Comic Sans MS
		JButton btnTexttofontcomic = new JButton("Comic Sans MS");
		btnTexttofontcomic.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int currentFontSize = lblHeadline.getFont().getSize();
				lblHeadline.setFont(new Font("Comic Sans MS", Font.PLAIN, currentFontSize));
			}
		});
		btnTexttofontcomic.setBounds(124, 201, 103, 23);
		contentPane.add(btnTexttofontcomic);
		
		// Font auf Courier New
		JButton btnTexttofontcourier = new JButton("Courier New");
		btnTexttofontcourier.setBounds(237, 201, 104, 23);
		btnTexttofontcourier.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int currentFontSize = lblHeadline.getFont().getSize();
				lblHeadline.setFont(new Font("Courier New", Font.PLAIN, currentFontSize));
			}
		});
		contentPane.add(btnTexttofontcourier);
		
		// Texteingabefeld
		JTextPane txtpnTextinput = new JTextPane();
		txtpnTextinput.setText("Hier bitte Text eingeben");
		txtpnTextinput.setToolTipText("Hier Text eingeben");
		txtpnTextinput.setBounds(10, 235, 331, 37);
		contentPane.add(txtpnTextinput);
		
		// Text in Label (ganz oben) schreiben
		JButton btnTexttolabel = new JButton("Ins Label schreiben");
		btnTexttolabel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblHeadline.setText(txtpnTextinput.getText());
			}
		});
		btnTexttolabel.setBounds(10, 283, 160, 23);
		contentPane.add(btnTexttolabel);
		
		// Text in Label l�schen
		JButton btnDeletefromlabel = new JButton("Text im Label l\u00F6schen");
		btnDeletefromlabel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblHeadline.setText("");
			}
		});
		btnDeletefromlabel.setBounds(180, 283, 161, 23);
		contentPane.add(btnDeletefromlabel);
		
		// Farbe des Texts in Label �ndern auf...
		JLabel lblAufgabeSchriftfarbe = new JLabel("Aufgabe 3: Schriftfarbe \u00E4ndern");
		lblAufgabeSchriftfarbe.setBounds(10, 317, 331, 14);
		contentPane.add(lblAufgabeSchriftfarbe);
		
		// ...rot
		JButton btnTexttored = new JButton("Rot");
		btnTexttored.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblHeadline.setForeground(Color.RED);
			}
		});
		btnTexttored.setBounds(10, 342, 104, 23);
		contentPane.add(btnTexttored);
		
		// ...blau
		JButton btnTexttoblue = new JButton("Blau");
		btnTexttoblue.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblHeadline.setForeground(Color.BLUE);
			}
		});
		btnTexttoblue.setBounds(124, 342, 103, 23);
		contentPane.add(btnTexttoblue);
		
		// ...schwarz
		JButton btnTexttoblack = new JButton("Schwarz");
		btnTexttoblack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblHeadline.setForeground(Color.BLACK);
			}
		});
		btnTexttoblack.setBounds(237, 342, 104, 23);
		contentPane.add(btnTexttoblack);
		
		// Schriftgr��e in Label �ndern
		JLabel lblAufgabeSchriftgre = new JLabel("Aufgabe 4: Schriftgr\u00F6\u00DFe \u00E4ndern");
		lblAufgabeSchriftgre.setBounds(10, 376, 331, 14);
		contentPane.add(lblAufgabeSchriftgre);
		
		// Schrift gr��er "+"
		JButton btnTextsizeplus = new JButton("+");
		btnTextsizeplus.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int currentFontSize = lblHeadline.getFont().getSize();
				String currentFontType = lblHeadline.getFont().getFamily();
				lblHeadline.setFont(new Font(currentFontType, Font.PLAIN, currentFontSize + 1));
			}
		});
		btnTextsizeplus.setFont(new Font("Tahoma", Font.BOLD, 11));
		btnTextsizeplus.setBounds(10, 401, 160, 23);
		contentPane.add(btnTextsizeplus);
		
		// Schrift kleiner "-"
		JButton btnTextsizeminus = new JButton("-");
		btnTextsizeminus.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int currentFontSize = lblHeadline.getFont().getSize();
				String currentFontType = lblHeadline.getFont().getFamily();
				lblHeadline.setFont(new Font(currentFontType, Font.PLAIN, currentFontSize - 1));
			}
		});
		btnTextsizeminus.setFont(new Font("Tahoma", Font.BOLD, 11));
		btnTextsizeminus.setBounds(180, 401, 161, 23);
		contentPane.add(btnTextsizeminus);
		
		// Textausrichtung �ndern auf...
		JLabel lblAufgabeTextausrichtung = new JLabel("Aufgabe 5: Textausrichtung");
		lblAufgabeTextausrichtung.setBounds(10, 435, 331, 14);
		contentPane.add(lblAufgabeTextausrichtung);
		
		// ...linksb�ndig
		JButton btnTextalignleft = new JButton("linksb\u00FCndig");
		btnTextalignleft.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblHeadline.setHorizontalAlignment(SwingConstants.LEFT);
			}
		});
		btnTextalignleft.setBounds(10, 460, 104, 23);
		contentPane.add(btnTextalignleft);
		
		// ...zentriert
		JButton btnTextaligncenter = new JButton("zentriert");
		btnTextaligncenter.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblHeadline.setHorizontalAlignment(SwingConstants.CENTER);
			}
		});
		btnTextaligncenter.setBounds(124, 460, 103, 23);
		contentPane.add(btnTextaligncenter);
		
		// ...rechtsb�ndig
		JButton btnTextalignright = new JButton("rechtsb\u00FCndig");
		btnTextalignright.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblHeadline.setHorizontalAlignment(SwingConstants.RIGHT);
			}
		});
		btnTextalignright.setBounds(237, 460, 104, 23);
		contentPane.add(btnTextalignright);
		
		// Programm beenden
		JLabel lblAufgabeProgramm = new JLabel("Aufgabe 6: Programm beenden");
		lblAufgabeProgramm.setBounds(10, 507, 331, 14);
		contentPane.add(lblAufgabeProgramm);
		
		JButton btnExit = new JButton("Exit");
		btnExit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(1);
			}
		});
		btnExit.setBounds(10, 532, 331, 23);
		contentPane.add(btnExit);
	}

	protected void buttonRedBg_clicked() {
		this.contentPane.setBackground(Color.RED);
	}
	protected void buttonGreenBg_clicked() {
		this.contentPane.setBackground(Color.GREEN);
	}
	protected void buttonBlueBg_clicked() {
		this.contentPane.setBackground(Color.BLUE);
	}
	protected void buttonYellowBg_clicked() {
		this.contentPane.setBackground(Color.YELLOW);
	}
	protected void buttonGivencolorBg_clicked(Color color) {
		this.contentPane.setBackground(color);
	}
	
}
