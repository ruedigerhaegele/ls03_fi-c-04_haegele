import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;

/**
  *
  * Test - �bungsGui zum Abpr�fen der grundlegenden GUI-Funktionen
  *
  * @version 1.2 vom 1.11.2012
  * @author Haegele, FI-C-04
  */

public class Form_aendern_LF5 extends JFrame {
  // Anfang Attribute
  private JPanel pnl_hintergrund = new JPanel(null, true);
    private JLabel lbl_anzeige = new JLabel();
    private JLabel lbl_aufgabe1 = new JLabel();
    private JButton btn_rot = new JButton();
    private JButton btn_blau = new JButton();
    private JLabel lbl_aufgabe2 = new JLabel();
    private JTextField tfd_eingabe = new JTextField();
    private JButton btn_label_aendern = new JButton();
    private JButton btn_label_loeschen = new JButton();
    private JLabel lbl_aufgabe3 = new JLabel();
    private JButton btn_schrift_gelb = new JButton();
    private JLabel lbl_aufgabe4 = new JLabel();
    private JButton btn_links = new JButton();
    private JButton btn_zentriert = new JButton();
    private JButton btn_rechts = new JButton();
    private JLabel lbl_aufgabe5 = new JLabel();
    private JButton btn_beenden = new JButton();
    private JLabel lbl_ergebnis = new JLabel();
  // Ende Attribute

  public Form_aendern_LF5(String title) {
    // Frame-Initialisierung
    super(title);
    setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
    int frameWidth = 410; 
    int frameHeight = 605;
    setSize(410, 493);
    Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
    int x = (d.width - getSize().width) / 2;
    int y = (d.height - getSize().height) / 2;
    setLocation(x, y);
    setResizable(false);
    Container cp = getContentPane();
    cp.setLayout(null);
    // Anfang Komponenten

    pnl_hintergrund.setBounds(0, -32, 400, 600);
    cp.add(pnl_hintergrund);
    lbl_anzeige.setBounds(8, 40, 379, 57);
    lbl_anzeige.setText("Dieser Text soll ver�ndert werden.");
    lbl_anzeige.setHorizontalAlignment(SwingConstants.CENTER);
    pnl_hintergrund.add(lbl_anzeige);
    lbl_aufgabe1.setBounds(8, 96, 207, 20);
    lbl_aufgabe1.setText("Aufgabe 1: Hintergrundfarbe �ndern");
    pnl_hintergrund.add(lbl_aufgabe1);
    btn_rot.setBounds(8, 120, 147, 25);
    btn_rot.setText("Rot");
    btn_rot.setMargin(new Insets(2, 2, 2, 2));
    btn_rot.addActionListener(new ActionListener() { 
      public void actionPerformed(ActionEvent evt) { 
        btn_rot_ActionPerformed(evt);
      }
    });
    btn_rot.setBackground(Color.RED);
    pnl_hintergrund.add(btn_rot);
    btn_blau.setBounds(8, 152, 147, 25);
    btn_blau.setText("Blau");
    btn_blau.setMargin(new Insets(2, 2, 2, 2));
    btn_blau.addActionListener(new ActionListener() { 
      public void actionPerformed(ActionEvent evt) { 
        btn_blau_ActionPerformed(evt);
      }
    });
    btn_blau.setBackground(Color.RED);
    pnl_hintergrund.add(btn_blau);
    lbl_aufgabe2.setBounds(8, 184, 162, 20);
    lbl_aufgabe2.setText("Aufgabe 2: Text eingeben");
    pnl_hintergrund.add(lbl_aufgabe2);
    tfd_eingabe.setBounds(8, 208, 374, 20);
    tfd_eingabe.setText("Hier bitte Text eingeben");
    pnl_hintergrund.add(tfd_eingabe);
    btn_label_aendern.setBounds(8, 240, 187, 25);
    btn_label_aendern.setText("Ins Label schreiben");
    btn_label_aendern.setMargin(new Insets(2, 2, 2, 2));
    btn_label_aendern.addActionListener(new ActionListener() { 
      public void actionPerformed(ActionEvent evt) { 
        btn_label_aendern_ActionPerformed(evt);
      }
    });
    btn_label_aendern.setBackground(Color.RED);
    pnl_hintergrund.add(btn_label_aendern);
    btn_label_loeschen.setBounds(200, 240, 179, 25);
    btn_label_loeschen.setText("Text im Label l�schen");
    btn_label_loeschen.setMargin(new Insets(2, 2, 2, 2));
    btn_label_loeschen.addActionListener(new ActionListener() { 
      public void actionPerformed(ActionEvent evt) { 
        btn_label_loeschen_ActionPerformed(evt);
      }
    });
    btn_label_loeschen.setBackground(Color.RED);
    pnl_hintergrund.add(btn_label_loeschen);
    lbl_aufgabe3.setBounds(8, 272, 178, 20);
    lbl_aufgabe3.setText("Aufgabe 3: Schriftfarbe �ndern");
    pnl_hintergrund.add(lbl_aufgabe3);
    btn_schrift_gelb.setBounds(8, 296, 187, 25);
    btn_schrift_gelb.setText("Gelb");
    btn_schrift_gelb.setMargin(new Insets(2, 2, 2, 2));
    btn_schrift_gelb.addActionListener(new ActionListener() { 
      public void actionPerformed(ActionEvent evt) { 
        btn_schrift_gelb_ActionPerformed(evt);
      }
    });
    btn_schrift_gelb.setBackground(Color.RED);
    pnl_hintergrund.add(btn_schrift_gelb);
    lbl_aufgabe4.setBounds(10, 332, 273, 20);
    lbl_aufgabe4.setText("Aufgabe 4: Textausrichtung (SwingConstants.  )");
    pnl_hintergrund.add(lbl_aufgabe4);
    btn_links.setBounds(10, 352, 107, 25);
    btn_links.setText("linksb�ndig");
    btn_links.setMargin(new Insets(2, 2, 2, 2));
    btn_links.addActionListener(new ActionListener() { 
      public void actionPerformed(ActionEvent evt) { 
        btn_links_ActionPerformed(evt);
      }
    });
    btn_links.setBackground(Color.RED);
    pnl_hintergrund.add(btn_links);
    btn_zentriert.setBounds(125, 352, 115, 25);
    btn_zentriert.setText("zentriert");
    btn_zentriert.setMargin(new Insets(2, 2, 2, 2));
    btn_zentriert.addActionListener(new ActionListener() { 
      public void actionPerformed(ActionEvent evt) { 
        btn_zentriert_ActionPerformed(evt);
      }
    });
    btn_zentriert.setBackground(Color.RED);
    pnl_hintergrund.add(btn_zentriert);
    btn_rechts.setBounds(250, 352, 123, 25);
    btn_rechts.setText("rechtsb�ndig");
    btn_rechts.setMargin(new Insets(2, 2, 2, 2));
    btn_rechts.addActionListener(new ActionListener() { 
      public void actionPerformed(ActionEvent evt) { 
        btn_rechts_ActionPerformed(evt);
      }
    });
    btn_rechts.setBackground(Color.RED);
    pnl_hintergrund.add(btn_rechts);
    lbl_aufgabe5.setBounds(8, 388, 180, 20);
    lbl_aufgabe5.setText("Aufgabe 5: Programm beenden");
    pnl_hintergrund.add(lbl_aufgabe5);
    btn_beenden.setBounds(8, 411, 379, 73);
    btn_beenden.setText("EXIT");
    btn_beenden.setMargin(new Insets(2, 2, 2, 2));
    btn_beenden.addActionListener(new ActionListener() { 
      public void actionPerformed(ActionEvent evt) { 
        btn_beenden_ActionPerformed(evt);
      }
    });
    btn_beenden.setBackground(Color.RED);
    pnl_hintergrund.add(btn_beenden);
    lbl_ergebnis.setBounds(264, 352, 110, 20);
    lbl_ergebnis.setText("");
    pnl_hintergrund.add(lbl_ergebnis);
    // Ende Komponenten

    setVisible(true);
  }

  // Anfang Methoden
  public void btn_rot_ActionPerformed(ActionEvent evt) {
	  this.pnl_hintergrund.setBackground(Color.RED);
  }

  public void btn_blau_ActionPerformed(ActionEvent evt) {
	  this.pnl_hintergrund.setBackground(Color.BLUE);
  }

  public void btn_label_aendern_ActionPerformed(ActionEvent evt) {
	  this.lbl_anzeige.setText(tfd_eingabe.getText());
  }

  public void btn_label_loeschen_ActionPerformed(ActionEvent evt) {
	  this.lbl_anzeige.setText("");
  }

  public void btn_schrift_gelb_ActionPerformed(ActionEvent evt) {
	  this.lbl_anzeige.setForeground(Color.YELLOW);
  }

  public void btn_links_ActionPerformed(ActionEvent evt) {
	  this.lbl_anzeige.setHorizontalAlignment(SwingConstants.LEFT);
  }

  public void btn_zentriert_ActionPerformed(ActionEvent evt) {
	  this.lbl_anzeige.setHorizontalAlignment(SwingConstants.CENTER);
  }

  public void btn_rechts_ActionPerformed(ActionEvent evt) {
	  this.lbl_anzeige.setHorizontalAlignment(SwingConstants.RIGHT);
  }

  public void btn_beenden_ActionPerformed(ActionEvent evt) {
	  System.exit(1);
  }

  // Ende Methoden

  public static void main(String[] args) {
    new Form_aendern_LF5("Form_aendern");
  }
}
